package com.chroom.order.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CondimentTest {

    private Condiment condiment;

    @BeforeEach
    public void setUp() {
        condiment = new Condiment("Susu", "Susu sapi segar.", 5000);
    }

    @Test
    public void testEmptyConstructor() {
        condiment = new Condiment();

        condiment.setName("Madu");
        assertEquals("Madu", condiment.getName());
    }

    @Test
    public void testGetId() {
        assertEquals(0, condiment.getId());
    }

    @Test
    public void testSetId() {
        condiment.setId(2);
        assertEquals(2, condiment.getId());
    }

    @Test
    public void testGetName() {
        assertEquals("Susu", condiment.getName());
    }

    @Test
    public void testSetName() {
        condiment.setName("Madu");
        assertEquals("Madu", condiment.getName());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Susu sapi segar.", condiment.getDescription());
    }

    @Test
    public void testSetDescription() {
        condiment.setDescription("Madu asli.");
        assertEquals("Madu asli.", condiment.getDescription());
    }

    @Test
    public void testGetPrice() {
        assertEquals(5000, condiment.getPrice());
    }

    @Test
    public void testSetPrice() {
        condiment.setPrice(6000);
        assertEquals(6000, condiment.getPrice());
    }

}
