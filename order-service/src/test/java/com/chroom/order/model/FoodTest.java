package com.chroom.order.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FoodTest {

    private Food food;

    @BeforeEach
    public void setUp() {
        food = new Food("Nasi Goreng", "Nasi digoreng", 12000);
    }

    @Test
    public void testEmptyConstructor() {
        food = new Food();

        food.setName("Pizza");
        assertEquals("Pizza", food.getName());
    }

    @Test
    public void testGetId() {
        assertEquals(0, food.getId());
    }

    @Test
    public void testSetId() {
        food.setId(2);
        assertEquals(2, food.getId());
    }

    @Test
    public void testGetName() {
        assertEquals("Nasi Goreng", food.getName());
    }

    @Test
    public void testSetName() {
        food.setName("Bakso");
        assertEquals("Bakso", food.getName());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Nasi digoreng", food.getDescription());
    }

    @Test
    public void testSetDescription() {
        food.setDescription("Daging sapi direbus.");
        assertEquals("Daging sapi direbus.", food.getDescription());
    }

    @Test
    public void testGetPrice() {
        assertEquals(12000, food.getPrice());
    }

    @Test
    public void testSetPrice() {
        food.setPrice(15000);
        assertEquals(15000, food.getPrice());
    }
}
