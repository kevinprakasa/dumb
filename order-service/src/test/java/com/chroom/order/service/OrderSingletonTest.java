package com.chroom.order.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.chroom.order.model.Condiment;
import com.chroom.order.model.Food;
import com.chroom.order.model.FoodOrder;
import com.chroom.order.model.Order;
import com.chroom.order.repository.CondimentRepository;
import com.chroom.order.repository.FoodOrderRepository;
import com.chroom.order.repository.FoodRepository;
import com.chroom.order.repository.OrderRepository;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OrderSingletonTest {

    @Mock
    private FoodRepository foodRepository;
    @Mock
    private CondimentRepository condimentRepository;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private FoodOrderRepository foodOrderRepository;

    @InjectMocks
    private OrderSingleton orderSingleton;

    @Test
    public void testNoPublicConstructors() throws Exception {
        Class<?> orderSingletonClass =
            Class.forName(OrderSingleton.class.getName());
        List<Constructor<?>> constructors =
            Arrays.asList(orderSingletonClass.getDeclaredConstructors());

        boolean check = constructors.stream().anyMatch(c ->
                !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void whenFindAllFoodIsCalledShouldCallFindAll() {
        orderSingleton.findAllFood();
        verify(foodRepository, times(1)).findAll();
    }

    @Test
    public void whenCreateEmptyOrderIsCalledShouldCallSave() {
        Order order = orderSingleton.createEmptyOrder();
        verify(orderRepository, times(1)).save(order);
    }

    @Test
    public void whenCreateFoodOrderIsCalledShouldCallFindById() {
        Food food = new Food("Nasi Goreng", "Nasi digoreng.", 14000);
        when(foodRepository.findById(0L)).thenReturn(Optional.of(food));

        FoodOrder foodOrder = orderSingleton.createFoodOrder(0L, 1);
        verify(foodRepository, times(1)).findById(0L);
        verify(condimentRepository, times(0)).findById(0L);
        verify(foodOrderRepository, times(1)).save(foodOrder);
    }

    @Test
    public void whenCreateFoodOrderIsCalledWithCondimentsShouldCallFindById() {
        Food food = new Food("Nasi Goreng", "Nasi digoreng.", 14000);
        when(foodRepository.findById(0L)).thenReturn(Optional.of(food));
        Condiment sausage = new Condiment("Sosis", "Sosis goreng.", 2000);
        when(condimentRepository.findById(0L)).thenReturn(Optional.of(sausage));

        FoodOrder foodOrder =
            orderSingleton.createFoodOrder(0L, new long[] {0L}, 1);
        verify(foodRepository, times(1)).findById(0L);
        verify(condimentRepository, times(1)).findById(0L);
        verify(foodOrderRepository, times(1)).save(foodOrder);
    }

    @Test
    public void whenCreateFoodIsCalledShouldCallCreateFood() {
        Food food =
            orderSingleton.createFood("Nasi Goreng", "Nasi Digoreng.", 14000);
        verify(foodRepository, times(1)).save(food);
    }

    @Test
    public void whenAddFoodOrderIsCalledShouldCallSave() {
        Order order = new Order();
        when(orderRepository.findById(0L)).thenReturn(Optional.of(order));
        Food food = new Food("Nasi Goreng", "Nasi digoreng.", 14000);
        when(foodRepository.findById(0L)).thenReturn(Optional.of(food));

        orderSingleton.addFoodOrder(0L, 0L, 1);

        verify(orderRepository, times(1)).save(order);
    }

    @Test
    public void whenAddFoodFromUserShouldSaveFoodOrder() {
        Food food = new Food("Nasi Goreng", "Nasi digoreng.", 14000);
        when(foodRepository.findById(2L)).thenReturn(Optional.of(food));
        when(foodRepository.findById(2L)).thenReturn(Optional.of(food));

        FoodOrder foodOrder = orderSingleton.addFoodFromUser(1, 2);

        verify(foodOrderRepository).save(foodOrder);
    }

    @Test
    public void whenDeleteFoodFromUserShouldRemoveFoodOrder() {
        Food food = new Food("Nasi Goreng", "Nasi digoreng.", 14000);
        food.setId(2);

        Order order = new Order(1L);
        FoodOrder foodOrder =
            new FoodOrder(food, new ArrayList<Condiment>(), 1);
        order.addFoodOrder(foodOrder);
        when(orderRepository.findByIdUser(1L)).thenReturn(Optional.of(order));

        orderSingleton.deleteFoodFromUser(1, 2);

        verify(foodOrderRepository).delete(foodOrder);
    }

    @Test
    public void whenGetCurrentOrderShouldCallFindByIdUser() {
        orderSingleton.getCurrentOrder(1L);

        verify(orderRepository).findByIdUser(1L);
    }

}
