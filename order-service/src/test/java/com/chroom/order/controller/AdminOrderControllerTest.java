package com.chroom.order.controller;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.chroom.order.model.Food;
import com.chroom.order.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = AdminOrderController.class)
public class AdminOrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderSevice;

    @Test
    public void addFood() throws Exception {
        Food food = new Food("Sate", "Sate Fasilkom", 12000);

        ObjectMapper objectMapper = new ObjectMapper();
        String inputJson = objectMapper.writeValueAsString(food);

        mockMvc.perform(post("/admin/add-food")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("addFood"));

        verify(orderSevice).createFood("Sate", "Sate Fasilkom", 12000);
    }

}
