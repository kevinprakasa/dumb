package com.chroom.order.service;

import com.chroom.order.model.Condiment;
import com.chroom.order.model.Food;
import com.chroom.order.model.FoodOrder;
import com.chroom.order.model.Order;
import com.chroom.order.repository.CondimentRepository;
import com.chroom.order.repository.FoodOrderRepository;
import com.chroom.order.repository.FoodRepository;
import com.chroom.order.repository.OrderRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public final class OrderSingleton implements OrderService {

    private static final OrderSingleton INSTANCE = new OrderSingleton();

    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private CondimentRepository condimentRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private FoodOrderRepository foodOrderRepository;

    private OrderSingleton() {
    }

    public static OrderSingleton getInstance() {
        return INSTANCE;
    }

    public Food createFood(String name, String description, int price) {
        Food food = new Food(name, description, price);
        foodRepository.save(food);
        return food;
    }

    public FoodOrder createFoodOrder(long foodId, int amount) {
        return this.createFoodOrder(foodId, new long[] {}, amount);
    }

    public FoodOrder createFoodOrder(long foodId, long[] condimentIds,
            int amount) {

        Food food = foodRepository.findById(foodId).get();
        List<Condiment> condiments = this.getCondimentsFromIds(condimentIds);

        FoodOrder foodOrder = new FoodOrder(food, condiments, amount);

        foodOrderRepository.save(foodOrder);
        return foodOrder;
    }

    public List<Food> findAllFood() {
        return foodRepository.findAll();
    }

    public Order createEmptyOrder() {
        Order order = new Order();
        orderRepository.save(order);
        return order;
    }

    public FoodOrder addFoodOrder(long orderId, long foodId, int amount) {
        Order order = orderRepository.findById(orderId).get();
        FoodOrder foodOrder = this.createFoodOrder(foodId, amount);
        order.addFoodOrder(foodOrder);
        orderRepository.save(order);
        return foodOrder;
    }

    private List<Condiment> getCondimentsFromIds(long[] condimentIds) {
        ArrayList<Condiment> condimentList = new ArrayList<>();

        for (long condimentId : condimentIds) {
            Condiment condiment = condimentRepository
                .findById(condimentId).get();
            condimentList.add(condiment);
        }

        return condimentList;
    }

    @Override
    public FoodOrder addFoodFromUser(long userId, long foodId) {
        Order order = this.getCurrentOrder(userId);

        for (FoodOrder foodOrder: order.getFoodOrderSet()) {
            if (foodOrder.getFood().getId() == foodId) {
                int currentAmount = foodOrder.getAmount();
                foodOrder.setAmount(currentAmount + 1);
                foodOrderRepository.save(foodOrder);
                return foodOrder;
            }
        }

        Food food = foodRepository.findById(foodId).get();
        FoodOrder foodOrder =
            new FoodOrder(food, new ArrayList<Condiment>(), 1);
        foodOrderRepository.save(foodOrder);

        Set<FoodOrder> foodOrders = order.getFoodOrderSet();
        foodOrders.add(foodOrder);

        orderRepository.save(order);
        return foodOrder;
    }

    @Override
    public FoodOrder deleteFoodFromUser(long userId, long foodId) {
        Order order = this.getCurrentOrder(userId);
        Set<FoodOrder> foodOrders = order.getFoodOrderSet();

        System.out.println(foodOrders);
        for (FoodOrder foodOrder: foodOrders) {
            System.out.println(foodOrder.getFood().getId());
            if (foodOrder.getFood().getId() == foodId) {
                int currentAmount = foodOrder.getAmount();
                foodOrder.setAmount(currentAmount - 1);

                if (currentAmount == 1) {
                    foodOrders.remove(foodOrder);
                    orderRepository.save(order);
                    foodOrderRepository.delete(foodOrder);
                } else {
                    foodOrderRepository.save(foodOrder);
                }

                return foodOrder;
            }
        }

        return null;
    }

    @Override
    public Order getCurrentOrder(long userId) {
        Optional<Order> orderOpt = orderRepository.findByIdUser(userId);

        if (orderOpt.isPresent()) {
            return orderOpt.get();
        } else {
            Order order = new Order(userId);
            orderRepository.save(order);
            return order;
        }
    }

}
