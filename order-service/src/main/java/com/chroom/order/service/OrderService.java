package com.chroom.order.service;

import com.chroom.order.model.Food;
import com.chroom.order.model.FoodOrder;
import com.chroom.order.model.Order;
import java.util.List;

public interface OrderService {

    List<Food> findAllFood();

    Food createFood(String name, String description, int price);

    FoodOrder addFoodFromUser(long userId, long foodId);

    FoodOrder deleteFoodFromUser(long userId, long foodId);

    Order getCurrentOrder(long userId);

}
