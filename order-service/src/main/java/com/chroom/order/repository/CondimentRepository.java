package com.chroom.order.repository;

import com.chroom.order.model.Condiment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CondimentRepository extends JpaRepository<Condiment, Long> {
}
