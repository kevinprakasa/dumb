package com.chroom.order.controller;

import com.chroom.order.model.Food;
import com.chroom.order.service.OrderService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/admin")
public class AdminOrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/add-food")
    public ResponseEntity<Map<String, String>> addFood(@RequestBody Food food) {
        orderService.createFood(food.getName(), food.getDescription(), food.getPrice());

        Map<String, String> map = new HashMap<>();
        map.put("status", "ok");
        return ResponseEntity.ok(map);
    }

}
