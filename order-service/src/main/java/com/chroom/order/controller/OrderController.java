package com.chroom.order.controller;

import com.chroom.order.model.Food;
import com.chroom.order.model.FoodOrder;
import com.chroom.order.model.Order;
import com.chroom.order.service.OrderService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<List<Food>> menu() {
        return ResponseEntity.ok(orderService.findAllFood());
    }

    @PostMapping("/{userId}/add/{foodId}")
    public ResponseEntity<FoodOrder> addFood(
            @PathVariable long userId,
            @PathVariable long foodId) {
        FoodOrder foodOrder = orderService.addFoodFromUser(userId, foodId);
        return ResponseEntity.ok(foodOrder);
    }

    @PostMapping("/{userId}/delete/{foodId}")
    public ResponseEntity<FoodOrder> deleteFood(
            @PathVariable long userId,
            @PathVariable long foodId) {
        FoodOrder foodOrder = orderService.deleteFoodFromUser(userId, foodId);
        return ResponseEntity.ok(foodOrder);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<Order> getOrder(@PathVariable long userId) {
        return ResponseEntity.ok(orderService.getCurrentOrder(userId));
    }
}
