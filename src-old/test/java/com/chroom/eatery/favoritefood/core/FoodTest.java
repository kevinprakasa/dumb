package com.chroom.eatery.favoritefood.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FoodTest {
    private Food food;
    private Customer customer;
    private Customer customer2;
    private ArrayList<Customer> custs;

    @BeforeEach
    public void setUp() {
        food = new Food("Ayam Goyeng", "Enak", 1000);
        customer = new Customer("Udin");
        customer2 = new Customer("Udin2");
        custs = new ArrayList<Customer>();
        custs.add(customer2);
    }

    @Test
    public void testGetName() {
        assertEquals("Ayam Goyeng", food.getName());
    }

    @Test
    public void testSetName() {
        food.setName("Ikan goyeng");
        assertEquals("Ikan goyeng", food.getName());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Enak", food.getDescription());
    }

    @Test
    public void testSetDescription() {
        food.setDescription("Gaenak");
        assertEquals("Gaenak", food.getDescription());
    }

    @Test
    public void testGetPrice() {
        assertEquals(1000, food.getPrice());
    }

    @Test
    public void testSetPrice() {
        food.setPrice(5000);
        assertEquals(5000, food.getPrice());
    }

    @Test
    public void testGetSubscribers() {
        assertTrue(food.getSubscribers() instanceof ArrayList);
    }

    @Test
    public void testSetSubscribers() {
        food.setSubscribers(custs);
        assertEquals(custs, food.getSubscribers());
    }

    @Test
    public void testAddSubscriber() {
        food.addSubscriber(customer);
        assertEquals("Udin", food.getSubscribers().get(0).getName());
    }

    @Test
    public void testRemoveSubscriber() {
        food.removeSubscriber(customer2);
        assertEquals(0, food.getSubscribers().size());
    }
}
