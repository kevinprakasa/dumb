package com.chroom.eatery.favoritefood.core;

public class Customer {
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String priceUpdate() {
        return "There has been a price change on your favorite food!";
    }

    public String menuUpdate() {
        return "Check out our brand new menu!";
    }
}
