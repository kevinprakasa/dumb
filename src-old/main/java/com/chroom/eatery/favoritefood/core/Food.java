package com.chroom.eatery.favoritefood.core;

import java.util.ArrayList;

public class Food {
    private String name;
    private String description;
    private int price;
    private ArrayList<Customer> subscribers = new ArrayList<Customer>();

    public Food(String name, String description, int price) {
        this.name = name;
        this.description = description;
        this.price = price;
        notifyNewMenu();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
        notifyPriceChange();
    }

    public ArrayList<Customer> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(ArrayList<Customer> subscribers) {
        this.subscribers = subscribers;
    }

    public void addSubscriber(Customer subscriber) {
        subscribers.add(subscriber);
    }

    public void removeSubscriber(Customer subscriber) {
        subscribers.remove(subscriber);
    }

    public void notifyPriceChange() {
        for (Customer customer : subscribers) {
            customer.priceUpdate();
        }
    }

    public void notifyNewMenu() {
        for (Customer customer : subscribers) {
            customer.menuUpdate();
        }
    }
}
