$(document).ready(() => {
    getData(); 
})

$(function () {
    $('#setOrder').click(function() {
        $.ajax({
            method: 'GET',
            url : '/track-admin/set-order',
            dataType: 'json',
            success: function(response) {
                $('#orderId').text(response.current_order_status_id);
                $('#orderState').text(response.current_order_status_state);
                $('#orderMessage').text(response.message);
            
                if(response.current_order_status_state != '-') {
                    $('#setOrder').attr('disabled',true);
                    $('#prepareOrder').attr('disabled',false);
                    $('#cookingOrder').attr('disabled',true);
                    $('#finishOrder').attr('disabled',true);
                }
            }
        })
    })
})

$(function () {
    $('#prepareOrder').click(function() {
        $('#prepareOrder').attr('disabled',true);
        $('#cookingOrder').attr('disabled',false);
        $.ajax({
            method: 'GET',
            url : '/track-admin/prepare-order',
            dataType: 'json',
            success: function(response) {
                $('#orderId').text(response.current_order_status_id);
                $('#orderState').text(response.current_order_status_state);
                $('#orderMessage').text(response.message);
            }
        })
    })
})

$(function () {
    $('#cookingOrder').click(function() {
        $('#cookingOrder').attr('disabled',true);
        $('#finishOrder').attr('disabled',false);
        $.ajax({
            method: 'GET',
            url : '/track-admin/cooking-order',
            dataType: 'json',
            success: function(response) {
                $('#orderId').text(response.current_order_status_id);
                $('#orderState').text(response.current_order_status_state);
                $('#orderMessage').text(response.message);
            }
        })
    })
})

$(function () {
    $('#finishOrder').click(function() {
        $('#setOrder').attr('disabled',false);
        $('#finishedOrder').attr('disabled',true);
        $.ajax({
            method: 'GET',
            url : '/track-admin/finished-cooking-order',
            dataType: 'json',
            success: function(response) {
                $('#orderId').text(response.current_order_status_id);
                $('#orderState').text(response.current_order_status_state);
                $('#orderMessage').text(response.message);
            }
        })
    })
})

function getData() {
    $.ajax({
        method: 'GET',
        url : '/track-admin/current-order',
        dataType: 'json',
        success: function(response) {
            $('#orderId').text(response.current_order_status_id);
            $('#orderState').text(response.current_order_status_state);
            $('#orderMessage').text(response.message);

            if(response.current_order_status_state === '-') {
                $('#prepareOrder').attr('disabled',true);
                $('#cookingOrder').attr('disabled',true);
                $('#finishOrder').attr('disabled',true);
            } else if(response.current_order_status_state === 'Order In') {
                $('#setOrder').attr('disabled',true);
                $('#prepareOrder').attr('disabled',false);
                $('#cookingOrder').attr('disabled',true);
                $('#finishOrder').attr('disabled',true);
            } else if(response.current_order_status_state === 'Prepare') {
                $('#setOrder').attr('disabled',true);
                $('#prepareOrder').attr('disabled',true);
                $('#cookingOrder').attr('disabled',false);
                $('#finishOrder').attr('disabled',true);
            } else if(response.current_order_status_state === 'Cooking') {
                $('#setOrder').attr('disabled',true);
                $('#prepareOrder').attr('disabled',true);
                $('#cookingOrder').attr('disabled',true);
                $('#finishOrder').attr('disabled',false);
            } else if(response.current_order_status_state === 'Completed') {
                $('#setOrder').attr('disabled',false);
                $('#prepareOrder').attr('disabled',true);
                $('#cookingOrder').attr('disabled',true);
                $('#finishOrder').attr('disabled',true);
            }
        }
    })
}