$(document).ready(() => {
    let status = document.getElementById("status").innerHTML;
    $("#statusPesanan").hide();
    $("#cancel").hide();
    if(status != '-') {
        $("#add").attr('disabled',true);
        $("#cancel").show(); 

        $.ajax({
            method: "GET",
            url: "/order-api/order/",
            dataType: "json",
            success: function (response) {
              console.log(response);
                                 
              $("#statusPesanan").show();  

              for (foodOrder of response.foodOrderSet) {
                let food = foodOrder.food;
                let row = $('<div class="row" />');
                row.append('<div class="col-1">' + food.id + "</div>");
                row.append('<div class="col-3">' + food.name + "</div>");
                row.append('<div class="col-5">' + food.description + "</div>");
                row.append('<div class="col-1">' + food.price + "</div>");
                row.append('<div class="col-1">' + foodOrder.amount + "</div>");
            
                $("#statusPesanan").append(row);
              }
        
              $("#statusPesanan").append(
                '<div class="row mt-5"><div class="col-1">Total</div><div class="col-3">' +
                  response.totalPrice +
                  "</div></div>"
              );
            }
        })
    }

    $(finish).hide();

    if(status === 'Order In') {
        $(statuspicture).append('<img id="ordinpic" src="/pic/track/ordin.svg" alt ="">');
    } else if (status === 'Prepare') {
        $(statuspicture).append('<img id="ordinpic" src="/pic/track/prep.svg" alt ="">');
    } else if (status === 'Cooking') {
        $(statuspicture).append('<img id="ordinpic" src="/pic/track/cook.svg" alt ="">');
        $(cancel).attr('disabled',true);
    } else if (status === 'Completed') {
        $(statuspicture).append('<img id="ordinpic" src="/pic/track/finish.svg" alt ="">');
        $(cancel).attr('disabled',true);
        $(finish).show();
    } 
})