$(document).ready(() => {
  $.ajax({
    method: "GET",
    url: "/order-api/",
    dataType: "json",
    success: function (response) {
      console.log(response);

      $("#menu").empty();
      for (let food of response) {
        let row = $('<div class="row" />');
        row.append('<div class="col-1">' + food.id + "</div>");
        row.append('<div class="col-3">' + food.name + "</div>");
        row.append('<div class="col-5">' + food.description + "</div>");
        row.append('<div class="col-1">' + food.price + "</div>");
        row.append('<div class="col-1">' + "0" + "</div>");
        row.append(
          '<div class="col-1 text-primary action" onclick="add(' +
            food.id +
            ')">add</div>'
        );
        $("#menu").append(row);
      }
    },
  });
});

function add(id) {
  $.ajax({
    method: "GET",
    url: "/order-api/add/" + id,
    dataType: "json",
    success: function (response) {
      console.log(id, response);
    },
    error: function (a, b) {
      console.log(a, b);
    },
  });
}
