$(document).ready(() => {
  getOrders();
});

function getOrders() {
  $.ajax({
    method: "GET",
    url: "/order-api/order/",
    dataType: "json",
    success: function (response) {
      console.log(response);

      $("#menu").empty();
      for (foodOrder of response.foodOrderSet) {
        let food = foodOrder.food;
        let row = $('<div class="row" />');
        row.append('<div class="col-1">' + food.id + "</div>");
        row.append('<div class="col-3">' + food.name + "</div>");
        row.append('<div class="col-5">' + food.description + "</div>");
        row.append('<div class="col-1">' + food.price + "</div>");
        row.append('<div class="col-1">' + foodOrder.amount + "</div>");
        row.append(
          '<div class="col-1 text-danger action" onclick="deleteOrder(' +
            food.id +
            ')">delete</div>'
        );
        $("#menu").append(row);
      }

      $("#menu").append(
        '<div class="row mt-5"><div class="col-1">Total</div><div class="col-3">' +
          response.totalPrice +
          "</div></div>"
      );

      $("#buy").attr("href", "/track-customer/add-order/" + response.id);
    },
  });
}

function deleteOrder(id) {
  $.ajax({
    method: "GET",
    url: "/order-api/delete/" + id,
    dataType: "json",
    success: function (response) {
      console.log(id, response);
      getOrders();
    },
    error: function (a, b) {
      console.log(a, b);
    },
  });
}
