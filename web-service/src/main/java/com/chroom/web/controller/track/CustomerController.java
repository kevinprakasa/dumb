package com.chroom.web.controller.track;

import com.netflix.discovery.converters.Auto;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("track-customer")
public class CustomerController {
    @Auto
    RestTemplate restTemplate;

    public CustomerController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public String customer(Model model) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer", Map.class);
        String orderStatus = map.get("order_status");

        model.addAttribute("order_id", map.get("order_id"));
        model.addAttribute("order_status", orderStatus);

        return "track/customer";
    }

    @GetMapping("/add-order/{orderId}")
    public String addOrder(
            @PathVariable("orderId") long orderId, RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/add-order/" + orderId, Map.class);
        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer";
    }

    @GetMapping("/cancel-order")
    public String cancelOrder(RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/cancel-order", Map.class);
        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer";
    }

    @GetMapping("/finish-order")
    public String finishOrder(RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/finish-order", Map.class);
        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer";
    }
}
