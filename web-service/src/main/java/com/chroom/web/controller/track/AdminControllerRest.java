package com.chroom.web.controller.track;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping(path = "/track-admin")
public class AdminControllerRest {
    @Autowired
    RestTemplate restTemplate;

    public AdminControllerRest(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/current-order")
    public Map<String, String> currentOrder() {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin", Map.class);
        return map;
    }

    @GetMapping("/set-order")
    public Map<String, String> setOrder() {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/set-order", Map.class);
        return map;
    }

    @GetMapping("/prepare-order")
    public Map<String, String> prepareOrder(RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/prepare-order", Map.class);

        return map;
    }

    @GetMapping("/cooking-order")
    public Map<String, String> cookingOrder(RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/cooking-order", Map.class);

        return map;
    }

    @GetMapping("/finished-cooking-order")
    public Map<String, String> finishedCookingOrder(RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/finished-cooking-order", Map.class);

        return map;
    }
}
