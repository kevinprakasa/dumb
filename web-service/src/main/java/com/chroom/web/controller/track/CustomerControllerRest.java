package com.chroom.web.controller.track;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "/track-customer")
public class CustomerControllerRest {
    @Autowired
    RestTemplate restTemplate;

    public CustomerControllerRest(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/current-order")
    public Map<String, String> currentOrder() {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer", Map.class);
        return map;
    }
}
