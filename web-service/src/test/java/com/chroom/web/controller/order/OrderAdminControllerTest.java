package com.chroom.web.controller.order;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@WebMvcTest(value = OrderAdminController.class)
public class OrderAdminControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void admin() throws Exception {
        mockMvc.perform(get("/admin"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("admin"));
    }

    @Test
    public void addFood() throws Exception {
        mockMvc.perform(get("/admin/add-food"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("addFood"));
    }

    @Test
    public void addFoodPost() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("name", "Sate");
        params.add("description", "Sate Fasilkom");
        params.add("price", "12000");

        mockMvc.perform(post("/admin/add-food").params(params))
            .andExpect(status().is3xxRedirection())
            .andExpect(handler().methodName("addFood"));
    }

}
