package com.chroom.eatery.account.controller;

import com.chroom.eatery.account.core.UserCreationException;
import com.chroom.eatery.account.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/account")
public class AccountController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String home(Model model) {
        Authentication auth =
            SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("username", auth.getName());
        return "account/home";
    }

    @GetMapping("/login")
    public String login() {
        return "account/login";
    }

    @GetMapping("/logout")
    public String logout() {
        return "account/logout";
    }

    @GetMapping("/register")
    public String registerCustomer() {
        return "account/register";
    }

    @PostMapping("/register")
    public String registerCustomer(Model model,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) {
        try {
            userService.register("user", username, password, name, address);
        } catch (UserCreationException e) {
            model.addAttribute("error", e);
            return "account/register";
        }
        return "redirect:/account/";
    }

    @GetMapping("/register/admin")
    public String registerAdmin(Model model) {
        return "account/register-admin";
    }

    @PostMapping("/register/admin")
    public String registerAdmin(Model model,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) {
        try {
            userService.register("admin", username, password, name, address);
        } catch (UserCreationException e) {
            model.addAttribute("error", e);
            return "account/register";
        }
        return "redirect:/account/";
    }

}
