package com.chroom.eatery.account.service;

import com.chroom.eatery.account.core.UserCreationException;
import com.chroom.eatery.account.core.UserFactory;
import com.chroom.eatery.account.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private UserFactory userFactory;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User register(String type, String username, String password,
            String name, String address) throws UserCreationException {
        String encodedPassword = passwordEncoder.encode(password);

        User user = null;
        user = userFactory.createUser(type, username, encodedPassword,
                    name, address);

        // auto login after register
        UsernamePasswordAuthenticationToken authToken =
            new UsernamePasswordAuthenticationToken(username, password);
        Authentication auth = authManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return user;
    }

}
