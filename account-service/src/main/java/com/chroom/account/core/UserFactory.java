package com.chroom.eatery.account.core;

import com.chroom.eatery.account.model.Role;
import com.chroom.eatery.account.model.User;
import com.chroom.eatery.account.repository.RoleRepository;
import com.chroom.eatery.account.repository.UserRepository;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFactory {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    public User createUser(String type, String username, String password,
            String name, String address) throws UserCreationException {

        if (userRepository.findByUsername(username).isPresent()) {
            throw new UserCreationException("Username already exist.");
        }

        Set<Role> roles = new HashSet<>();

        switch (type) {
            case "admin":
                roles.add(roleRepository.findByName("ROLE_ADMIN").get());
                break;
            case "user":
                roles.add(roleRepository.findByName("ROLE_USER").get());
                break;
            default:
                throw new UserCreationException("User type '" + type
                        + "' not exist.");
        }

        User user = new User(username, password, name, roles, address);
        userRepository.save(user);

        return user;
    }

}
