package com.chroom.track.model;

public class CanceledState implements OrderState {
    private String canceled = "Order canceled";

    public CanceledState(OrderStatus orderStatus) {}

    public String prepare() {
        return canceled;
    }

    public String cooking() {
        return canceled;
    }

    public String finishCooking() {
        return canceled;
    }

    public String getState() {
        return "Your order has been canceled";
    }

    public String cancelOrder() {
        return canceled;
    }

    public String toString() {
        return "Canceled";
    }
}
