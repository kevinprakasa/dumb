package com.chroom.track.model;

public class CompletedState implements OrderState {
    private String complete = "Order Completed";

    public CompletedState(OrderStatus orderStatus) {}

    public String prepare() {
        return complete;
    }

    public String cooking() {
        return complete;
    }

    public String finishCooking() {
        return complete;
    }

    public String getState() {
        return "Your order is now finished, thank you!";
    }

    public String cancelOrder() {
        return complete;
    }

    public String toString() {
        return "Completed";
    }
}
