package com.chroom.track.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_status")
public class OrderStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private Long orderId;

    @Column
    private String currentState;

    @Column
    private String admin;

    @Column
    private String customer;

    public OrderStatus(){}

    public OrderStatus(long orderId) {
        this.orderId = orderId;
        System.out.println(this.orderId);
        currentState = "order in";
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getAdmin() {
        return this.admin;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomer() {
        return this.customer;
    }

    public OrderState getState() {
        switch (this.currentState) {
            case "prepare":
                return new PrepareState(this);
            case "cooking":
                return new CookingState(this);
            case "completed":
                return new CompletedState(this);
            case "canceled":
                return new CanceledState(this);
            default :
                return new OrderInState(this);
        }
    }

    public void setState(String currentState) {
        this.currentState = currentState;
    }

    public String prepare() {
        return getState().prepare();
    }

    public String cooking() {
        return getState().cooking();
    }

    public String finishCooking() {
        return getState().finishCooking();
    }

    public String cancelOrder() {
        return getState().cancelOrder();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
