package com.chroom.track.service;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CustomerService {
    @Autowired
    private OrderStatusRepository orderStatusRepository;

    private OrderStatus orderStatus;

    public String addOrder(long orderId) {
        if (this.orderStatus == null) {
            this.orderStatus = new OrderStatus(orderId);
            this.orderStatus.setCustomer("customer");
            orderStatusRepository.save(this.orderStatus);
            return orderStatus.getState().toString();
        } else {
            return "Your order isn't done yet";
        }
    }

    public String checkOrderStatus() throws OrderNotFoundException {
        try {
            this.orderStatus = orderStatusRepository.getOne(this.orderStatus.getId());
            return this.orderStatus.getState().toString();
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("You haven't ordered anything");
        }
    }

    public String cancelOrder() throws OrderNotFoundException {
        try {
            this.orderStatus = orderStatusRepository.getOne(this.orderStatus.getId());
            String message = this.orderStatus.cancelOrder();
            if (message.equals("Order canceled")) {
                if (this.orderStatus.getAdmin() == null) {
                    orderStatusRepository.delete(this.orderStatus);
                } else {
                    orderStatusRepository.save(this.orderStatus);
                }
                this.orderStatus = null;
            }
            return message;
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("You haven't ordered anything");
        }
    }

    public String finishOrder() throws OrderNotFoundException {
        try {
            if (this.orderStatus.getState().toString().equals("Completed")) {
                orderStatusRepository.save(this.orderStatus);
                this.orderStatus = null;
                return "Order is complete";
            } else {
                return "Your order isn't completed yet";
            }
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("You haven't ordered anything");
        }
    }

    public long getOrderId() throws OrderNotFoundException {
        try {
            return this.orderStatus.getId();
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("You haven't ordered anything");
        }
    }
}
