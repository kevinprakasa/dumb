package com.chroom.track.service;

import com.chroom.track.model.CanceledState;
import com.chroom.track.model.OrderState;
import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    @Autowired
    private OrderStatusRepository orderStatusRepository;
    private OrderStatus currentOrderStatus;

    public OrderStatus getCurrentOrder() throws OrderNotFoundException {
        try {
            List<OrderStatus> orderStatusList = orderStatusRepository.findAll();
            int index = 0;
            OrderStatus orderStatus = orderStatusList.get(index);
            while (orderStatus.getAdmin() != null) {
                index++;
                orderStatus = orderStatusList.get(index);
            }
            return orderStatus;
        } catch (IndexOutOfBoundsException err) {
            throw new OrderNotFoundException("No order yet");
        }
    }

    public OrderStatus setOrder() throws OrderNotFoundException {
        try {
            if (this.currentOrderStatus == null) {
                this.currentOrderStatus = getCurrentOrder();
                this.currentOrderStatus.setAdmin("admin");
                orderStatusRepository.save(this.currentOrderStatus);
            }
            return currentOrderStatus;
        } catch (OrderNotFoundException err) {
            throw new OrderNotFoundException("No order yet");
        }
    }

    public void cancelOrder(OrderStatus orderStatus) {
        if (this.currentOrderStatus.equals(orderStatus)) {
            orderStatusRepository.delete(this.currentOrderStatus);
            this.currentOrderStatus = null;
        }
    }

    public String prepareOrder() throws OrderNotFoundException {
        try {
            if (getCurrentOrderStatusString().equals("Order Canceled")) {
                return "Order Canceled";
            }
            String message = currentOrderStatus.prepare();
            orderStatusRepository.save(this.currentOrderStatus);
            return message;
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("Set an order first!");
        }
    }

    public String cookingOrder() throws OrderNotFoundException {
        try {
            if (getCurrentOrderStatusString().equals("Order Canceled")) {
                return "Order Canceled";
            }
            String message = currentOrderStatus.cooking();
            orderStatusRepository.save(this.currentOrderStatus);
            return message;
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("Set an order first!");
        }
    }

    public String finishedCookingOrder() throws OrderNotFoundException {
        try {
            if (getCurrentOrderStatusString().equals("Order Canceled")) {
                return "Order Canceled";
            }
            String status = currentOrderStatus.finishCooking();
            long orderId = currentOrderStatus.getId();
            if (status.equalsIgnoreCase("Order completed")) {
                orderStatusRepository.save(this.currentOrderStatus);
                currentOrderStatus = null;
            }
            return "Order with ID : " + orderId + "is finished";
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("Set an order first!");
        }
    }


    public String getCurrentOrderStatusString()throws OrderNotFoundException {
        try {
            this.currentOrderStatus = orderStatusRepository
                    .getOne(this.currentOrderStatus.getId());
            OrderState orderState = currentOrderStatus.getState();
            if (orderState instanceof CanceledState) {
                cancelOrder(this.currentOrderStatus);
                return "Order Canceled";
            }
            return orderState.toString();
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("Set an order first!");
        }
    }

    public long getCurrentOrderStatusId() throws OrderNotFoundException {
        try {
            this.currentOrderStatus = orderStatusRepository
                    .getOne(this.currentOrderStatus.getId());
            return this.currentOrderStatus.getId();
        } catch (NullPointerException err) {
            throw new OrderNotFoundException("No order yet");
        }
    }
}
