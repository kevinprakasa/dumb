package com.chroom.track.repository;

import com.chroom.track.model.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderStatusRepository
        extends JpaRepository<OrderStatus, Long> {
}
