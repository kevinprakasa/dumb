package com.chroom.track.controller;

import com.chroom.track.service.AdminService;
import com.chroom.track.service.OrderNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/track-admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @GetMapping
    public Map<String, String> admin() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString());
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId()));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    "Handle an order please");
        }
        return map;
    }

    @GetMapping("/set-order")
    public Map<String, String> setOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            adminService.setOrder();
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString());
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId()));
            map.put("message",
                    "Order has been set");
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    "No order yet");
        }
        return map;
    }

    @GetMapping("/prepare-order")
    public Map<String, String> prepareOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message",
                    adminService.prepareOrder());
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString());
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId()));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }

    @GetMapping("/cooking-order")
    public Map<String, String> cookingOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message",
                    adminService.cookingOrder());
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString());
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId()));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }

    @GetMapping("/finished-cooking-order")
    public Map<String, String> finishedCookingOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    adminService.finishedCookingOrder());
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }
}
