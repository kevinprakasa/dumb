package com.chroom.track.controller;

import com.chroom.track.service.CustomerService;
import com.chroom.track.service.OrderNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/track-customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping
    public Map<String, String> customer() {
        Map<String, String> map = new HashMap<>();
        map.put("message", "");
        try {
            map.put("order_id",
                    String.valueOf(customerService.getOrderId()));
            map.put("order_status",
                    customerService.checkOrderStatus());
        } catch (OrderNotFoundException err) {
            map.put("order_id",
                    "-");
            map.put("order_status",
                    "-");
            map.put("message",
                    "You haven't ordered anything");
        }
        return map;
    }

    @GetMapping("/add-order/{orderId}")
    public Map<String, String> addOrder(
            @PathVariable("orderId") long orderId
    ) throws OrderNotFoundException {
        Map<String, String> map = new HashMap<>();
        map.put("message", customerService.addOrder(orderId));
        map.put("order_id", String.valueOf(customerService.getOrderId()));
        map.put("order_status", customerService.checkOrderStatus());
        return map;
    }

    @GetMapping("/cancel-order")
    public Map<String, String> cancelOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message", customerService.cancelOrder());
        } catch (OrderNotFoundException err) {
            map.put("message", "You haven't ordered anything");
        }
        return map;
    }

    @GetMapping("/finish-order")
    public Map<String, String> finishOrder() {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message", customerService.finishOrder());
        } catch (OrderNotFoundException err) {
            map.put("message", "You haven't ordered anything");
        }
        return map;
    }
}
