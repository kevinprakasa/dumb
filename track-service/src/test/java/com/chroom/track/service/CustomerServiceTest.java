package com.chroom.track.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @Mock
    private OrderStatusRepository orderStatusRepository;

    @InjectMocks
    private CustomerService customerService;

    @InjectMocks
    private AdminService adminService;

    private OrderStatus orderStatus;

    @BeforeEach
    public void setUp() {
        orderStatus = new OrderStatus(1);
        when(orderStatusRepository.save(any(OrderStatus.class))).thenReturn(orderStatus);
        lenient().when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);
    }

    @Test
    @Order(1)
    public void testAddOrder() {
        assertEquals("Order In", customerService.addOrder(1));
        assertEquals("Your order isn't done yet", customerService.addOrder(2));
    }

    @Test
    @Order(2)
    public void testCheckOrderStatus() throws OrderNotFoundException {
        try {
            assertEquals("You haven't ordered anything", customerService.checkOrderStatus());
        } catch (OrderNotFoundException err) {
            customerService.addOrder(1);
            assertEquals("Order In", customerService.checkOrderStatus());

            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            adminService.setOrder();
            adminService.prepareOrder();
            assertEquals("Prepare", adminService.getCurrentOrderStatusString());
            assertEquals("Prepare", customerService.checkOrderStatus());
        }
    }

    @Test
    @Order(3)
    public void testCancelOrder() throws OrderNotFoundException {
        try {
            customerService.cancelOrder();
        } catch (OrderNotFoundException err) {
            customerService.addOrder(1);
            customerService.cancelOrder();
            try {
                customerService.checkOrderStatus();
            } catch (OrderNotFoundException err2) {
                customerService.addOrder(2);
                assertEquals("Canceled", customerService.checkOrderStatus());
            }
        }
    }

    @Test
    @Order(5)
    public void testGetOrderId() throws OrderNotFoundException {
        try {
            customerService.getOrderId();
        } catch (OrderNotFoundException err) {
            customerService.addOrder(1);
            assertEquals(0, customerService.getOrderId());
        }
    }

    @Test
    @Order(6)
    public void testFinishOrder() throws OrderNotFoundException {
        try {
            customerService.finishOrder();
        } catch (OrderNotFoundException err) {
            customerService.addOrder(1);
            assertEquals("Your order isn't completed yet", customerService.finishOrder());
        }
    }
}
