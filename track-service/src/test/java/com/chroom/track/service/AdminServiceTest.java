package com.chroom.track.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AdminServiceTest {
    @Mock
    private OrderStatusRepository orderStatusRepository;

    @InjectMocks
    private AdminService adminService;

    private OrderStatus orderStatus;
    private OrderStatus currentOrder;


    @Test
    @Order(1)
    public void testGetCurrentOrder() throws OrderNotFoundException {
        try {
            currentOrder = adminService.getCurrentOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);

            currentOrder = adminService.getCurrentOrder();
            assertEquals(orderStatus, currentOrder);
        }
    }

    @Test
    @Order(2)
    public void testSetOrder() throws OrderNotFoundException {
        try {
            currentOrder = adminService.setOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);

            long orderStatusId = adminService.setOrder().getId();
            assertEquals(adminService.getCurrentOrderStatusId(), orderStatusId);

            adminService.setOrder();
        }
    }

    @Test
    @Order(3)
    public void testCancelOrder() throws OrderNotFoundException {
        try {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);

            adminService.setOrder();
            assertEquals(orderStatus.getId(), adminService.getCurrentOrderStatusId());

            adminService.cancelOrder(orderStatus);
            assertEquals(0, adminService.getCurrentOrderStatusId());

            adminService.cancelOrder(orderStatus);
        } catch (OrderNotFoundException err) {
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(new OrderStatus(1));
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);

            adminService.setOrder();
            assertEquals("Order In", adminService.getCurrentOrderStatusString());
        }
    }

    @Test
    @Order(4)
    public void testPrepareOrder() throws OrderNotFoundException {
        try {
            adminService.prepareOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);

            adminService.setOrder();
            assertEquals("Order In", adminService.getCurrentOrderStatusString());
            adminService.prepareOrder();
            assertEquals("Prepare", adminService.getCurrentOrderStatusString());
        }
    }

    @Test
    @Order(5)
    public void testCookingOrder() throws OrderNotFoundException {
        try {
            adminService.cookingOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);

            adminService.setOrder();
            adminService.prepareOrder();
            adminService.cookingOrder();
            assertEquals("Cooking", adminService.getCurrentOrderStatusString());
        }
    }

    @Test
    @Order(6)
    public void testFinishedCookingOrder() throws OrderNotFoundException {
        try {
            adminService.finishedCookingOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus(1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);
            when(orderStatusRepository.getOne(any())).thenReturn(orderStatus);

            adminService.setOrder();
            adminService.prepareOrder();
            adminService.cookingOrder();
            adminService.finishedCookingOrder();

            try {
                adminService.getCurrentOrderStatusString();
            } catch (OrderNotFoundException err2) {
                orderStatusList.add(new OrderStatus(1));
                adminService.setOrder();
                assertEquals("Completed", adminService.getCurrentOrderStatusString());
            }
        }
    }
}
