package com.chroom.track.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import com.chroom.track.service.AdminService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = AdminController.class)
public class AdminControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdminService adminService;

    @Test
    public void setOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/set-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("setOrder"));
    }

    @Test
    public void prepareOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/prepare-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("prepareOrder"));
    }

    @Test
    public void cookingOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/cooking-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("cookingOrder"));
    }

    @Test
    public void finishedCookingOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/finished-cooking-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("finishedCookingOrder"));
    }
}
