package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PrepareStateTest {
    private PrepareState prepareState;

    @BeforeEach
    public void setUp() {
        prepareState = new PrepareState(new OrderStatus(1));
    }

    @Test
    public void testPrepare() {
        assertEquals("Preparing", prepareState.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("Cooking order", prepareState.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("You have to cook first!", prepareState.finishCooking());
    }

    @Test
    public void getStateTest() {
        assertEquals("Currently preparing your order!",
                prepareState.getState());
    }

    @Test
    public void cancelOrderTest() {
        assertEquals("Order canceled", prepareState.cancelOrder());
    }
}
