package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderStatusTest {
    private OrderStatus orderStatus;

    @BeforeEach
    public void setUp() {
        orderStatus = new OrderStatus(1);
    }

    @Test
    public void testPrepare() {
        assertEquals("Preparing", orderStatus.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("You have to prepare first!", orderStatus.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("You have to prepare first!", orderStatus.finishCooking());
    }

    @Test
    public void testCancelOrder() {
        assertEquals("Order canceled", orderStatus.cancelOrder());
    }

    @Test
    public void testGetState() {
        assertTrue(orderStatus.getState() instanceof OrderInState);
        orderStatus.prepare();
        assertTrue(orderStatus.getState() instanceof PrepareState);
        orderStatus.cooking();
        assertTrue(orderStatus.getState() instanceof CookingState);
        orderStatus.finishCooking();
        assertTrue(orderStatus.getState() instanceof CompletedState);

        OrderStatus orderStatus1 = new OrderStatus(1);
        orderStatus1.cancelOrder();
        assertTrue(orderStatus1.getState() instanceof CanceledState);
    }

    @Test
    public void testGetSetId() {
        orderStatus.setId(1234);
        assertEquals(1234, orderStatus.getId());
    }
}
